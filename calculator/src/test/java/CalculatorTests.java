import com.tora.Calculator;
import org.junit.Before;
import org.junit.Test;


public class CalculatorTests {

    Calculator calculator;
    int a, b, c, d;

    @Before
    public void setup() {
        calculator = new Calculator();
        a = 2;
        b = 3;
        c = 4;
        d = 5;
    }

    @Test
    public void testAdd() {
        assert (calculator.add(a, b) == 5);
    }

    @Test
    public void testSub() {
        assert (calculator.sub(a, d) == -3);
    }

    @Test
    public void testMul() {
        assert (calculator.mul(a, d) == 10);

    }

    @Test
    public void testDiv() {
        assert (calculator.div(d, a) == 2.5);

    }
}
