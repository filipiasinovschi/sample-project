package com.tora;

public class Calculator {

    public int add(int a, int b) {
        return a + b;
    }

    public int sub(int a, int b) {
        return a - b;
    }

    public int mul(int a, int b) {
        return a * b;
    }

    public double div(double a,double b){
        if(b==0)
            throw new RuntimeException();
        return  a/b;

    }
}
