package com.tora;

import java.util.Scanner;

public class Console {
    public void run() {
        Calculator calculator = new Calculator();
        int a, b;

        while (true) {
            System.out.println("enter an operation: ");
            Scanner scanner = new Scanner(System.in);
            String operation = scanner.next();

            if (operation.equals("exit"))
                break;

            System.out.println("enter 2 numbers: ");
            {
                a = scanner.nextInt();
                b = scanner.nextInt();
            }

            switch (operation) {
                case ("add") -> {
                    System.out.println("The result is : " + calculator.add(a, b));
                }
                case ("sub") -> {
                    System.out.println("The result is : " + calculator.sub(a, b));
                }
                case ("mul") -> {
                    System.out.println("The result is : " + calculator.mul(a, b));
                }
                case ("div") -> {
                    try {
                        System.out.println("The result is : " + calculator.div(a, b));
                    } catch (Exception err) {
                        err.printStackTrace();
                    }
                }
                default -> {
                    System.out.println("Try another command");
                }
            }
        }

    }

}

